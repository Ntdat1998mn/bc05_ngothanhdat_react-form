import React, { Component } from "react";
import { connect } from "react-redux";
import Item from "./Item";

class List extends Component {
  renderListUser = () => {
    let result = this.props.userList.map((item, index) => {
      return <Item user={item} index={index} />;
    });
    return result;
  };
  renderSearchUser = () => {
    let result = this.props.searchUser.map((item, index) => {
      let searchKey = `S${index}`;
      return <Item user={item} index={searchKey} />;
    });
    return result;
  };

  // Xoá user

  render() {
    return (
      <div className="container">
        <table className="table border">
          <thead className="bg-black text-white">
            <tr>
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th className="text-center">Chỉnh sửa</th>
            </tr>
          </thead>
          <tbody>{this.renderListUser()}</tbody>
          <tfoot>
            <tr>
              <th className="text-center bg-black text-white" colSpan={5}>
                Kết quả tìm kiếm
              </th>
            </tr>
            {this.renderSearchUser()}
          </tfoot>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    userList: state.userReducer.userList,
    searchUser: state.userReducer.searchUser,
  };
};

export default connect(mapStateToProps)(List);

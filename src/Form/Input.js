import React, { Component } from "react";

import { connect } from "react-redux";
import { ADD_USER, SEARCH } from "./constant/constant";

class Input extends Component {
  constructor(props) {
    super(props);
    /* this.inputRef = React.createRef("sgdfgd"); */
  }
  state = {
    user: {
      code: "",
      name: "",
      phoneNumber: "",
      email: "",
    },
    error: {
      code: "",
      name: "",
      phoneNumber: "",
      email: "",
    },
    searchInput: "",
  };

  // Lấy thông tin từ thẻ input ở Form User
  handleGetUserForm = (e) => {
    let { value, name: key, placeholder } = e.target;

    let cloneUser = { ...this.state.user };
    let cloneError = { ...this.state.error };
    this.errorMessage = "";
    if (value.trim() == "") {
      this.errorMessage = `*${placeholder} không được rỗng`;
    }
    cloneUser[key] = value;
    cloneError[key] = this.errorMessage;
    this.setState({
      user: cloneUser,
      error: cloneError,
    });
  };
  // Lấy value từ thẻ input phần search
  handleGetInputForm = (e) => {
    let { value } = e.target;
    let cloneSearchInput = { ...this.state.searchInput };
    cloneSearchInput = value;
    this.setState({ ...this.state, searchInput: cloneSearchInput });
  };
  // Reset user Form
  onResetUserForm = () => {
    let cloneUser = { ...this.state.user };
    for (var key in cloneUser) {
      cloneUser[key] = "";
    }
    this.setState({ user: cloneUser });
  };
  // Xác nhận thêm User
  handleAddSubmit = (e) => {
    e.preventDefault();
    if (this.errorMessage == "" && this.checkValid()) {
      this.props.handleAddUser(this.state.user);
      this.onResetUserForm();
    }
  };
  // Xác nhận tìm thông tin tìm kiếm
  handleSearchSubmit = (e) => {
    e.preventDefault();
    this.props.handleSearch(this.state.searchInput);
  };
  // Kiểm tra
  checkValid = () => {
    let valid = true;
    let cloneUser = { ...this.state.user };
    let cloneError = { ...this.state.error };
    // Kiểm tra rỗng
    for (let key in cloneUser) {
      if (cloneUser[key] == "") {
        this.errorMessage = `*Bạn cần nhập đầy đủ thông tin`;
        cloneError[key] = this.errorMessage;
        this.setState({
          error: cloneError,
        });
        valid = false;
      }
      //Kiểm tra mã sinh viên
      else {
        if (key == "code") {
          let reg = /^\d{4,6}$/; // Mã SV từ 4 đến 6 số
          if (!reg.test(cloneUser[key])) {
            this.errorMessage = `*Mã sinh viên từ 4-6 số`;
            cloneError[key] = this.errorMessage;
            this.setState({
              error: cloneError,
            });
            valid = false;
          } else {
            this.props.userList.forEach((item) => {
              if (cloneUser.code == item.code) {
                this.errorMessage = `*Mã sinh viên không được trùng`;
                cloneError.code = this.errorMessage;
                this.setState({
                  error: cloneError,
                });
                valid = false;
              }
            });
          }
        }
        // Kiểm tra tên sinh viên
        if (key == "name") {
          let reg =
            /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
          if (!reg.test(cloneUser[key])) {
            this.errorMessage = `*Tên sinh viên không đúng định dạng`;
            cloneError[key] = this.errorMessage;
            this.setState({
              error: cloneError,
            });
          }
        }
        // Kiểm tra số điện thoại
        if (key == "phoneNumber") {
          let reg = /(0[3|5|7|8|9])+([0-9]{8})\b/g;
          if (!reg.test(cloneUser[key])) {
            this.errorMessage = `*Số điện thoại không đúng định dạng (Ex: 0355846457)`;
            cloneError[key] = this.errorMessage;
            this.setState({
              error: cloneError,
            });
          }
        }
      }
    }
    return valid;
  };

  render() {
    return (
      <div className="container">
        <div className="bg-black p-3 flex justify-between mb-3">
          <b className="text-white">Thông tin sinh viên</b>
          <form onSubmit={this.handleSearchSubmit} className="pl-5 w-50 flex">
            <input
              onChange={this.handleGetInputForm}
              className="text-black w-100 rounded px-2"
              type="text"
              name="input"
              placeholder="Nhập thông tin tìm kiếm"
            />
            <button type="submit" className="btn btn-success ml-2">
              Search
            </button>
          </form>
        </div>
        <form onSubmit={this.handleAddSubmit} className="form-group row">
          <div className="col-6">
            <label htmlFor="student-code">Mã SV</label>
            <input
              onChange={this.handleGetUserForm}
              type="text"
              className="form-control"
              name="code"
              placeholder="Mã sinh viên"
              /* ref={this.inputRef} */
              value={this.state.user.code}
            />
            <span className="text-danger" id="spanCode">
              {this.state.error.code}
            </span>
          </div>
          <div className="col-6">
            <label htmlFor="name">Họ tên</label>
            <input
              onChange={this.handleGetUserForm}
              type="text"
              className="form-control "
              name="name"
              placeholder="Tên sinh viên"
              /* ref={this.inputRef} */
              value={this.state.user.name}
            />
            <span className="text-danger" id="spanName">
              {this.state.error.name}
            </span>
          </div>
          <div className="col-6">
            <label htmlFor="phoneNumber-number">Số điện thoại</label>
            <input
              onChange={this.handleGetUserForm}
              type="text"
              className="form-control"
              name="phoneNumber"
              placeholder="Số điện thoại"
              /* ref={this.inputRef} */
              value={this.state.user.phoneNumber}
            />
            <span className="text-danger" id="spanPhoneNumber">
              {this.state.error.phoneNumber}
            </span>
          </div>
          <div className="col-6">
            <label htmlFor="email">Email</label>
            <input
              onChange={this.handleGetUserForm}
              type="email"
              className="form-control"
              name="email"
              placeholder="Email"
              /* ref={this.inputRef} */
              value={this.state.user.email}
            />
            <span className="text-danger" id="spanEmail">
              {this.state.error.email}
            </span>
          </div>
          <button
            type="submit"
            className="ml-3 mt-3 btn bg-success text-white p-2"
          >
            Thêm sinh viên
          </button>
        </form>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    userList: state.userReducer.userList,
    editUser: state.userReducer.user,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddUser: (newUser) => {
      let action = {
        type: ADD_USER,
        payload: newUser,
      };
      dispatch(action);
    },
    handleSearch: (input) => {
      let action = {
        type: SEARCH,
        payload: input,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Input);

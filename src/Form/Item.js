import React, { Component } from "react";
import { connect } from "react-redux";
import { REMOVE_USER, EDIT_USER } from "./constant/constant";

class Item extends Component {
  render() {
    return (
      <tr key={this.props.index}>
        <td>{this.props.user.code}</td>
        <td>{this.props.user.name}</td>
        <td>{this.props.user.phoneNumber}</td>
        <td>{this.props.user.email}</td>
        <td className="text-center">
          <button
            onClick={() => {
              this.props.editUser(this.props.index);
            }}
            className="btn btn-dark mr-2"
          >
            Sửa
          </button>
          <button
            onClick={() => {
              this.props.removeUser(this.props.user.code);
            }}
            className="btn btn-dark "
          >
            Xoá
          </button>
        </td>
      </tr>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    removeUser: (code) => {
      let action = {
        type: REMOVE_USER,
        payload: code,
      };
      dispatch(action);
    },
    editUser: (code) => {
      let action = {
        type: EDIT_USER,
        payload: code,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(Item);

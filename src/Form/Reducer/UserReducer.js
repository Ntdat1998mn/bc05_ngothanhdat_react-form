import { ADD_USER, REMOVE_USER, EDIT_USER, SEARCH } from "../constant/constant";
let initialState = {
  userList: [
    {
      code: 1,
      name: "Nguyễn Văn A",
      phoneNumber: "0337346998",
      email: "nguyenvana@gmail.com",
    },
    {
      code: 2,
      name: "Nguyễn Văn B",
      phoneNumber: "0337346999",
      email: "nguyenvanb@gmail.com",
    },
    {
      code: 3,
      name: "Nguyễn Văn B",
      phoneNumber: "0337346999",
      email: "nguyenvanb@gmail.com",
    },
  ],
  user: null,
  searchUser: [],
};
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER: {
      let cloneUserList = [...state.userList];
      cloneUserList.push(action.payload);
      return { ...state, userList: cloneUserList };
    }
    case SEARCH: {
      let cloneUserList = [...state.userList];

      let searchUser = cloneUserList.filter((item) => {
        return (
          item.code == action.payload.trim() ||
          item.name == action.payload.trim() ||
          item.phoneNumber == action.payload.trim() ||
          item.email == action.payload.trim()
        );
      });

      return { ...state, searchUser: searchUser };
    }
    case REMOVE_USER: {
      let cloneUserList = [...state.userList];
      let newUserList = cloneUserList.filter((item) => {
        return item.code !== action.payload;
      });
      return { ...state, searchUser: [], userList: newUserList };
    }
    case EDIT_USER: {
      let editUser = [...state.userList][action.payload];
      return { ...state, user: editUser };
    }
    default:
      return state;
  }
};

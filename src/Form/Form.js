import React, { Component } from "react";
import Input from "./Input";
import List from "./List";

class Form extends Component {
  render() {
    return (
      <div>
        <Input />
        <List />
      </div>
    );
  }
}
export default Form;
